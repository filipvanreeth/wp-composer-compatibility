<?php
/**
 * Composer Plugin Checker class.
 *
 * Checks if a plugin is available and installable with Composer.
 *
 * @package Filip_Van_Reeth\WP_Composer_Compatibility
 */

namespace Filip_Van_Reeth\WP_Composer_Compatibility;

/**
 * Class Composer_Plugin_Checker
 *
 * Checks if a plugin is available and installable with Composer.
 */
class Composer_Plugin_Checker {
	/**
	 * The path of the plugin.
	 *
	 * @var string $plugin_path
	 */
	private string $plugin_path;

	/**
	 * This private property holds the contents of the composer.json file.
	 *
	 * @var array
	 */
	private array $composer_json = array();
	/**
	 * Indicates whether the plugin is on wpackagist.
	 *
	 * @var bool $is_on_wpackagist True if the plugin is on wpackagist, false otherwise.
	 */
	private bool $is_on_wpackagist = false;
	/**
	 * Determines if the plugin is on Packagist.
	 *
	 * @var bool $is_on_packagist Whether the plugin is on Packagist or not.
	 */
	private bool $is_on_packagist = false;

	/**
	 * Constructor for the PluginChecker class.
	 *
	 * @param string $plugin_path The path to the plugin.
	 */
	public function __construct( string $plugin_path ) {
		$this->plugin_path      = $plugin_path;
		$this->composer_json    = $this->read_composer_json();
		$this->is_on_wpackagist = $this->is_available_on_wpackagist();
		$this->is_on_packagist  = $this->is_available_on_packagist();
	}

	/**
	 * Retrieves the path of the plugin.
	 *
	 * @return string The path of the plugin.
	 */
	public function get_plugin_path(): string {
		return $this->plugin_path;
	}

	/**
	 * Retrieves the composer.json file as an array.
	 *
	 * @return array The contents of the composer.json file.
	 */
	public function get_composer_json(): array {
		return $this->composer_json;
	}

	/**
	 * Returns true if the plugin is available on wpackagist, false otherwise.
	 *
	 * @return boolean
	 */
	public function is_on_wpackagist() {
		return $this->is_on_wpackagist;
	}

	/**
	 * Returns true if the plugin is available on Packagist, false otherwise.
	 *
	 * @return boolean
	 */
	public function is_on_packagist() {
		return $this->is_on_packagist;
	}

	/**
	 * Retrieves the plugin slug.
	 *
	 * @return string The plugin slug.
	 */
	private function get_plugin_slug() {
		if ( empty( $this->plugin_path ) ) {
			return '';
		}

		return basename( dirname( $this->get_plugin_path() ) );
	}

	/**
	 * Retrieves the path to the composer.json file.
	 *
	 * @return string The path to the composer.json file.
	 */
	private function get_composer_path(): string {
		return dirname( $this->get_plugin_path() );
	}

	/**
	 * Checks if the plugin has a composer.json file.
	 *
	 * @return boolean True if the plugin has a composer.json file, false otherwise.
	 */
	private function has_composer_json_file(): bool {
		return file_exists( $this->get_composer_json_file() );
	}

	/**
	 * Retrieves the path to the composer.json file.
	 *
	 * @return string The path to the composer.json file.
	 */
	private function get_composer_json_file(): string {
		return $this->get_composer_path() . '/composer.json';
	}

	/**
	 * Reads the composer.json file and returns its contents as an array.
	 *
	 * @return array The contents of the composer.json file.
	 */
	private function read_composer_json() {
		if ( ! $this->has_composer_json_file() ) {
			return array();
		}

		$composer_json_file = $this->get_composer_json_file();
		$composer_json      = file_get_contents( $composer_json_file ); //phpcs:ignore

		return json_decode( $composer_json, true );
	}

	/**
	 * Checks if the plugin is available on wpackagist.
	 *
	 * @return boolean Returns true if the plugin is available on wpackagist, false otherwise.
	 */
	private function is_available_on_wpackagist(): bool {
		$response = wp_remote_get( "https://api.wordpress.org/plugins/info/1.0/{$this->get_plugin_slug()}.json" );

		if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) !== 200 ) {
			return false;
		}

		return true;
	}

	/**
	 * Checks if the plugin is available on Packagist.
	 *
	 * @return boolean Returns true if the plugin is available on Packagist, false otherwise.
	 */
	private function is_available_on_packagist(): bool {
		$composer_json = $this->get_composer_json();

		if ( ! isset( $composer_json['name'] ) ) {
			return false;
		}

		$response = wp_remote_get( "https://packagist.org/packages/{$this->get_composer_json()['name']}.json" );

		if ( is_wp_error( $response ) || wp_remote_retrieve_response_code( $response ) !== 200 ) {
			return false;
		}

		return true;
	}

	/**
	 * Checks if the plugin is installable via Composer.
	 *
	 * @return boolean Returns true if the plugin is installable via Composer, false otherwise.
	 */
	public function is_composer_installable(): bool {
		return $this->is_available_on_wpackagist() || $this->is_available_on_packagist();
	}
}
