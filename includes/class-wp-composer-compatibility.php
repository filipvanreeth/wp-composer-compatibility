<?php
/**
 * Composer Plugin Checker class.
 *
 * Checks if a plugin is available and installable with Composer.
 *
 * @package Filip_Van_Reeth\WP_Composer_Compatibility
 */

namespace Filip_Van_Reeth\WP_Composer_Compatibility;

/**
 * Class WP_Composer_Compatibility
 *
 * Main plugin class.
 */
class WP_Composer_Compatibility {

	/**
	 * Class WP_Composer_Compatibility
	 *
	 * This class represents the WP_Composer_Compatibility plugin and provides a singleton instance.
	 *
	 * @var WP_Composer_Compatibility|null The instance of the WP_Composer_Compatibility class.
	 */
	private static $instance;

	/**
	 * Retrieves the instance of the WP_Composer_Compatibility class.
	 *
	 * @return WP_Composer_Compatibility The instance of the WP_Composer_Compatibility class.
	 */
	public static function get_instance() {
		if ( ! isset( self::$instance ) ) {
			self::$instance = new self();
		}

		return self::$instance;
	}
	/**
	 * Constructor for the WP_Composer_Compatibility class.
	 *
	 * @access private
	 */
	private function __construct() {
		add_action( 'plugins_loaded', array( $this, 'load_textdomain' ) );
	}

	/**
	 * Loads the text domain for the WP Composer Compatibility plugin.
	 */
	public function load_textdomain() {
		load_plugin_textdomain(
			'wp-composer-compatibility',
			false,
			dirname( plugin_basename( __FILE__ ) ) . '/languages'
		);
	}
}
