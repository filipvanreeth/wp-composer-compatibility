<?php
/**
 * Composer CLI class.
 *
 * @package Filip_Van_Reeth\WP_Composer_Compatibility
 */

namespace Filip_Van_Reeth\WP_Composer_Compatibility;

use WP_CLI_Command;
use WP_CLI;

/**
 * Class Composer_CLI
 *
 * This class extends the WP_CLI_Command class and provides additional functionality
 * for interacting with Composer in the WordPress environment.
 */
class Composer_CLI extends WP_CLI_Command {
	/**
	 * Checks plugins for composer compatibility.
	 *
	 * @return void
	 *
	 * ## EXAMPLES
	 *
	 *  # Check plugins for composer compatibility.
	 *  $ wp composer plugins-check
	 *
	 * @subcommand plugins-check
	 */
	public function plugins_check() {
		$plugins = get_plugins();

		if ( empty( $plugins ) ) {
			WP_CLI::error( 'No plugins found.' );
		}

		$count_plugins = count( $plugins );

		$cli_progress_bar = WP_CLI\Utils\make_progress_bar(
			'Checking plugins for composer compatibility...',
			$count_plugins
		);

		$table_items = array();

		$count_is_composer_installable = 0;

		$plugins = array_map(
			function ( $plugin ) use ( &$table_items, $cli_progress_bar, &$count_is_composer_installable ) {
				$plugin_path = WP_PLUGIN_DIR . '/' . $plugin;

				$wp_composer_plugin_checker = new Composer_Plugin_Checker( $plugin_path );

				if ( $wp_composer_plugin_checker->is_composer_installable() ) {
					$count_is_composer_installable++;
				}

				$plugin_data = get_plugin_data( $plugin_path );

				$table_items[] = array(
					'plugin'               => htmlspecialchars_decode( $plugin_data['Name'] ),
					'composer_json'        => $wp_composer_plugin_checker->get_composer_json() ? 'yes' : 'no',
					'wpackagist'           => $wp_composer_plugin_checker->is_on_wpackagist() ? 'yes' : 'no',
					'packagist'            => $wp_composer_plugin_checker->is_on_packagist() ? 'yes' : 'no',
					'composer_installable' => $wp_composer_plugin_checker->is_composer_installable() ? 'yes' : 'no',
				);

				$cli_progress_bar->tick();

				return $plugin;
			},
			array_keys( $plugins )
		);

		$cli_progress_bar->finish();

		WP_CLI\Utils\format_items(
			'table',
			$table_items,
			array( 'plugin', 'composer_json', 'wpackagist', 'packagist', 'composer_installable' )
		);

		WP_CLI::success( "{$count_is_composer_installable} out of {$count_plugins} plugins are composer installable." );
	}
}

if ( defined( 'WP_CLI' ) && WP_CLI ) {
	WP_CLI::add_command( 'composer', Composer_CLI::class );
}
