# Composer Compatibility for WordPress

This plugin allows you to check if the plugins you have installed are compatible and installable with Composer.

## Installation

### Via Composer

```bash
composer require --dev filipvanreeth/wp-composer-compatibility
```

## Usage

### Check if the plugins are compatible and installable with Composer

```bash
wp composer plugins-check
```
This command will check if the plugins you have installed are compatible and installable with Composer and will return the number of plugins that are compatible and installable with Composer.