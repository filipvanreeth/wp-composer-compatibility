<?php
/**
 * Main plugin file.
 *
 * @package Filip_Van_Reeth\WP_Composer_Compatibility
 */

/**
 * Plugin Name:       Composer Compatibility
 * Plugin URI:        https://gitlab.com/filipvanreeth/wp-composer-compatibility
 * Description:       WP-CLI command only. Checks if plugins are available and installable with Composer.
 * Version:           1.1.2
 * Requires at least: 6.0
 * Requires PHP:      8.0
 * Tested up to:      6.5.2
 * Author:            Filip Van Reeth
 * Author URI:        https://filipvanreeth.com
 * License:           MIT
 * Update URI:        https://gitlab.com/filipvanreeth/wp-composer-compatibility
 */

use Filip_Van_Reeth\WP_Composer_Compatibility\Composer_CLI;
use Filip_Van_Reeth\WP_Composer_Compatibility\WP_Composer_Compatibility;

require_once __DIR__ . '/includes/class-wp-composer-compatibility.php';
require_once __DIR__ . '/includes/class-composer-plugin-checker.php';

if ( defined( 'WP_CLI' ) && WP_CLI ) {
	require_once __DIR__ . '/includes/class-composer-cli.php';
	\WP_CLI::add_command( 'composer', Composer_CLI::class );
}

// Initialize the plugin.
WP_Composer_Compatibility::get_instance();
